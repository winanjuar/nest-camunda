import { Provider } from '@nestjs/common';
import { ClientConfig } from 'camunda-external-task-client-js';
export declare class ExternalTaskModule {
    static createClient(config: ClientConfig): Provider;
}
