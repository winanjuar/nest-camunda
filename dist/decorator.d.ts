import { SubscribeOptions } from 'camunda-external-task-client-js';
export declare const Subscription: (topic: string, options?: SubscribeOptions) => MethodDecorator;
