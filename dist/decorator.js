"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Subscription = void 0;
const microservices_1 = require("@nestjs/microservices");
const Subscription = (topic, options) => {
    return (...args) => {
        const messagePattern = (0, microservices_1.MessagePattern)({ topic, options: options || null });
        messagePattern(...args);
    };
};
exports.Subscription = Subscription;
//# sourceMappingURL=decorator.js.map