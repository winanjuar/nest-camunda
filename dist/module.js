"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExternalTaskModule = void 0;
const common_1 = require("@nestjs/common");
const camunda_external_task_client_js_1 = require("camunda-external-task-client-js");
const CONNECTION_PROVIDER = 'CONNECTION_PROVIDER';
let ExternalTaskModule = class ExternalTaskModule {
    static createClient(config) {
        return {
            provide: CONNECTION_PROVIDER,
            useFactory: async () => {
                return new camunda_external_task_client_js_1.Client(config);
            },
        };
    }
};
ExternalTaskModule = __decorate([
    (0, common_1.Module)({})
], ExternalTaskModule);
exports.ExternalTaskModule = ExternalTaskModule;
//# sourceMappingURL=module.js.map