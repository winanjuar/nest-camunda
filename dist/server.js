"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExternalTaskConnector = void 0;
const microservices_1 = require("@nestjs/microservices");
const common_1 = require("@nestjs/common");
const camunda_external_task_client_js_1 = require("camunda-external-task-client-js");
let ExternalTaskConnector = class ExternalTaskConnector extends microservices_1.Server {
    constructor(client) {
        super();
        this.client = client;
    }
    async listen(callback) {
        this.init();
        callback();
    }
    close() {
        this.client.stop();
        common_1.Logger.log('External Task Client stopped', 'ExternalTaskConnector');
    }
    init() {
        this.client.start();
        common_1.Logger.log('External Task Client started', 'ExternalTaskConnector');
        const handlers = this.getHandlers();
        handlers.forEach((messageHandler, key) => {
            const jsonKey = JSON.parse(key);
            this.client.subscribe(jsonKey.topic, jsonKey.options, ({ task, taskService }) => {
                messageHandler(task, taskService)
                    .then(console.log)
                    .catch(console.error);
            });
        });
    }
};
ExternalTaskConnector = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('CONNECTION_PROVIDER')),
    __metadata("design:paramtypes", [camunda_external_task_client_js_1.Client])
], ExternalTaskConnector);
exports.ExternalTaskConnector = ExternalTaskConnector;
//# sourceMappingURL=server.js.map