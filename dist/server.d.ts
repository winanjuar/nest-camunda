import { Server, CustomTransportStrategy } from '@nestjs/microservices';
import { Client } from 'camunda-external-task-client-js';
export declare class ExternalTaskConnector extends Server implements CustomTransportStrategy {
    private readonly client;
    constructor(client: Client);
    listen(callback: () => void): Promise<void>;
    close(): void;
    protected init(): void;
}
